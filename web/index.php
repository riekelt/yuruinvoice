<?php

require_once '../vendor/autoload.php';

$application = new \YuruInvoice\Application\YuruInvoice();
$application->serve(new \YuruInvoice\Framework\Http\Request($_SERVER));