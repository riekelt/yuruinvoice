<?php

namespace YuruInvoice\Framework\Http;

use YuruInvoice\Framework\Exception\Http\HeaderContainer\InvalidHeaderContainerSourceFormatException;

/**
 * Class HeaderContainer
 * @package YuruInvoice\Framework\Http
 */
class HeaderContainer
{
    /**
     * @var array
     */
    public $headers;

    /**
     * HeaderContainer constructor.
     * @param $source
     */
    public function __construct($source)
    {
        if (!is_array($source)) {
            throw new InvalidHeaderContainerSourceFormatException();
        }

        $this->headers = $source;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value)
    {
        $this->headers[$key] = $value;
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        if (!array_key_exists($key, $this->headers)) {
            return $default;
        }

        return $this->headers[$key];
    }
}