<?php

namespace YuruInvoice\Framework\Http;

/**
 * Class Response
 * @package YuruInvoice\Framework\Http
 */
class Response
{
    /**
     * @var HeaderContainer
     */
    public $headers;

    /**
     * @var string
     */
    public $output;

    /**
     * Response constructor.
     * @param $output
     * @param array $headers
     */
    public function __construct($output, $headers = [])
    {
        $this->output = $output;
        $this->headers = new HeaderContainer([]);
    }

    /**
     * Bon a petit! - Maika-chan 2k17
     * (Cycle complete yo)
     */
    public function respond()
    {
        // Write headers
        foreach ($this->headers->headers as $key => $value) {
            header($key . ': ' . $value);
        }

        // Write the response, and bon a petit
        die($this->output);
    }
}