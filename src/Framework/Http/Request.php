<?php

namespace YuruInvoice\Framework\Http;

use YuruInvoice\Framework\Exception\Http\Request\InvalidRequestSourceFormatException;

/**
 * Class Request
 * @package YuruInvoice\Framework\Http
 */
class Request
{
    /**
     * @var array
     */
    public $rawRequest;

    /**
     * @var string
     */
    public $method;

    /**
     * @var string
     */
    public $uri;

    /**
     * @var int
     */
    public $time;

    /**
     * @var HeaderContainer
     */
    public $headers;

    /**
     * Request constructor.
     * @param array $request
     */
    public function __construct($request)
    {
        if (
            !array_key_exists('REQUEST_METHOD', $request) ||
            !array_key_exists('REQUEST_URI', $request) ||
            !array_key_exists('REQUEST_TIME', $request)
        ) {
            throw new InvalidRequestSourceFormatException();
        }

        $this->rawRequest = $request;
        $this->method = $request['REQUEST_METHOD'];
        $this->uri = $request['REQUEST_URI'];
        $this->time = $request['REQUEST_TIME'];

        $this->headers = new HeaderContainer(getallheaders());
    }
}