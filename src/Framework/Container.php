<?php

namespace YuruInvoice\Framework;

use YuruInvoice\Framework\Exception\Container\ConflictingParameterKeyException;
use YuruInvoice\Framework\Exception\Container\ConflictingServiceKeyException;
use YuruInvoice\Framework\Exception\Container\NonExistentParameterRequestedException;
use YuruInvoice\Framework\Exception\Container\NonExistentServiceRequestedException;

/**
 * Class Container
 * @package YuruInvoice\Framework
 */
class Container
{
    /**
     * @var array
     */
    private $storage;

    /**
     * @var array
     */
    private $parameters;

    /**
     * Container constructor.
     */
    public function __construct()
    {
        $this->storage = [];
        $this->parameters = [];
    }

    /**
     * @param string $key
     * @return mixed
     * @throws NonExistentServiceRequestedException
     */
    public function get($key)
    {
        if (!array_key_exists($key, $this->storage)) {
            throw new NonExistentServiceRequestedException($key);
        }

        return $this->storage[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param bool $safe
     * @throws ConflictingServiceKeyException
     */
    public function set($key, $value, $safe = false)
    {
        if ($safe && array_key_exists($key, $this->storage)) {
            throw new ConflictingServiceKeyException($key);
        }

        $this->storage[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getParameter($key)
    {
        if (!array_key_exists($key, $this->parameters)) {
            throw new NonExistentParameterRequestedException($key);
        }

        return $this->parameters[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param bool $safe
     */
    public function setParameter($key, $value, $safe = false)
    {
        if ($safe && array_key_exists($key, $this->parameters)) {
            throw new ConflictingParameterKeyException($key);
        }

        $this->parameters[$key] = $value;
    }
}