<?php

namespace YuruInvoice\Framework\Exception\Container;

/**
 * Class ConflictingParameterKeyException
 * @package YuruInvoice\Framework\Exception\Container
 */
class ConflictingParameterKeyException extends \Exception
{
    /**
     * @var string
     */
    public $parameterKey;

    /**
     * ConflictingServiceKeyException constructor.
     * @param string $parameterKey
     */
    public function __construct($parameterKey)
    {
        $this->parameterKey = $parameterKey;
    }
}