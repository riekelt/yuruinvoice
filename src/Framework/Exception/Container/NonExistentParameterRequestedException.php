<?php

namespace YuruInvoice\Framework\Exception\Container;

/**
 * Class NonExistentParameterRequestedException
 * @package YuruInvoice\Framework\Exception\Container
 */
class NonExistentParameterRequestedException extends \Exception
{
    /**
     * @var string
     */
    public $parameterKey;

    /**
     * NonExistentParameterRequestedException constructor.
     * @param string $parameterKey
     */
    public function __construct($parameterKey)
    {
        $this->parameterKey = $parameterKey;
    }
}