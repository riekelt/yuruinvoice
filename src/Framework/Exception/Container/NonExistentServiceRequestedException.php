<?php
/**
 * Created by PhpStorm.
 * User: Riekelt Brands
 * Date: 17-1-2018
 * Time: 20:41
 */

namespace YuruInvoice\Framework\Exception\Container;

/**
 * Class NonExistentServiceRequestedException
 * @package YuruInvoice\Framework\Exception\Container
 */
class NonExistentServiceRequestedException extends \Exception
{
    /**
     * @var string
     */
    public $serviceKey;

    /**
     * NonExistentServiceRequestedException constructor.
     * @param string $requestedService
     */
    public function __construct($serviceKey)
    {
        $this->serviceKey = $serviceKey;
    }
}