<?php

namespace YuruInvoice\Framework\Exception\Container;

/**
 * Class ConflictingServiceKeyException
 * @package YuruInvoice\Framework\Exception\Container
 */
class ConflictingServiceKeyException extends \Exception
{
    /**
     * @var string
     */
    public $serviceKey;

    /**
     * ConflictingServiceKeyException constructor.
     * @param string $serviceKey
     */
    public function __construct($serviceKey)
    {
        $this->serviceKey = $serviceKey;
    }
}