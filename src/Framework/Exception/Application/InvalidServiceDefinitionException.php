<?php

namespace YuruInvoice\Framework\Exception\Application;

/**
 * Class InvalidServiceDefinitionException
 * @package YuruInvoice\Framework\Exception\Application
 */
class InvalidServiceDefinitionException extends \Exception
{
    /**
     * @var string
     */
    public $serviceKey;

    /**
     * @var string
     */
    public $serviceValue;

    /**
     * InvalidServiceDefinitionException constructor.
     * @param $serviceKey
     * @param $serviceValue
     */
    public function __construct($serviceKey, $serviceValue)
    {
        $this->serviceKey = $serviceKey;
        $this->serviceValue = $serviceValue;
    }
}