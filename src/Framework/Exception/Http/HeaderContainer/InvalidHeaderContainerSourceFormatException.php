<?php

namespace YuruInvoice\Framework\Exception\Http\HeaderContainer;

/**
 * Class InvalidHeaderContainerSourceFormatException
 * @package YuruInvoice\Framework\Exception\Http\HeaderContainer
 */
class InvalidHeaderContainerSourceFormatException extends \Exception
{

}