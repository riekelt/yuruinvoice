<?php

namespace YuruInvoice\Framework\Exception\Http\Request;

/**
 * Class InvalidRequestSourceFormatException
 * @package YuruInvoice\Framework\Exception\Http\Request
 */
class InvalidRequestSourceFormatException extends \Exception
{

}