<?php

namespace YuruInvoice\Framework;

use YuruInvoice\Framework\Exception\Application\InvalidParametersFileFormat;
use YuruInvoice\Framework\Exception\Application\InvalidServiceDefinitionException;
use YuruInvoice\Framework\Exception\Application\InvalidServicesFileFormat;

/**
 * Class Application
 * @package YuruInvoice\Framework
 */
class Application
{
    /**
     * @var Container
     */
    public $container;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->container = new Container();
    }

    /**
     * @param string $file
     */
    private function loadParameters($file)
    {
        $parsedFile = yaml_parse_file($file);

        if (!array_key_exists('parameters', $parsedFile)) {
            throw new InvalidParametersFileFormat();
        }

        $parameters = $parsedFile['parameters'];

        foreach ($parameters as $key => $value) {
            $this->container->setParameter($key, $value);
        }
    }

    /**
     * @param string $file
     */
    private function loadServices($file)
    {
        $parsedFile = yaml_parse_file($file);

        if (!array_key_exists('services', $parsedFile)) {
            throw new InvalidServicesFileFormat();
        }

        $services = $parsedFile['services'];

        foreach ($services as $key => $value) {
            if (!class_exists($value)) {
                throw new InvalidServiceDefinitionException($key, $value);
            }

            $class = new $value;

            if (method_exists($class, 'setContainer')) {
                $class->setContainer($this->container);
            }

            $this->container->set($key, $class);
        }
    }
}