<?php

namespace YuruInvoice\Application;

use YuruInvoice\Framework\Application;
use YuruInvoice\Framework\Http\Request;
use YuruInvoice\Framework\Http\Response;

/**
 * Class YuruInvoice
 * @package YuruInvoice\Application
 */
class YuruInvoice extends Application
{
    /**
     * YuruInvoice constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     */
    public function serve($request)
    {
        $response = new Response('Sugoi!');

        $response->respond();
    }
}